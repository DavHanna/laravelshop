<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>My app</title>
  </head>
  <body>
    <nav>
      <ul>
        <li>Home</li>
        <li>About</li>
      </ul>
    </nav>
    @yield('content')
  </body>
</html>
