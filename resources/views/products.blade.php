@extends('layout')

@section('content')

<table>
  @foreach($products as $product)
  <tr>
    <td>Name</td>
    <td>{{$product->name}}</td>
  </tr>
  <tr>
    <td>Price</td>
    <td>{{$product->price}}</td>
  </tr>
  @endforeach
</table>

@endsection
